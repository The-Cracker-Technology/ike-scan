autoreconf --install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Autoreconf... PASS!"
else
  # houston we have a problem
  exit 1
fi

./configure --with-openssl

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Configure... PASS!"
else
  # houston we have a problem
  exit 1
fi

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

make check

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make check... PASS!"
else
  # houston we have a problem
  exit 1
fi

make install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make install... PASS!"
else
  # houston we have a problem
  exit 1
fi
